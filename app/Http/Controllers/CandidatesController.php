<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;

// full name is "App\Http\Controllers\CandidatesController"; 
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $candidates = Candidate::all();
        return view('candidates.index', compact('candidates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     //הפונקציה שולחת אותנו אל הטופס
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     //ביצוע פעולות שישמרו את הנתונים
    public function store(Request $request)
    {
        $candidate = new Candidate();

        //לקדנדידס יש את השדה שם מהמודל
        // לריקווסט יש את השדה שם כי ישאב את הנתונים מהטופס
        $candidate->name = $request->name; 
        $candidate->email = $request->email;
        //שמירה במסד הנתונים
        $candidate->save();
        //מעביר את המשתמש חזרה לדף קנדידטס
        //מכניסים לפונקציה URL
        return redirect('candidates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //אם מנסים לערוך אובייקט שלא קיים (מתקבל איי.דיי שלא קיים)ת)
        $candidate = Candidate::findOrFail($id);
        return view('candidates.edit', compact('candidate','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $candidate = Candidate::findOrFail($id);
       //מעדנים עם הנתונים שהגיעו מהטופס
       //$candidate->update($request);
       $candidate->name = $request->name; 
       $candidate->email = $request->email;
       //$candidate->update($request->except(['_token']));
       $candidate->update();
       return redirect('candidates');
    }
//hyjykuikukuykukuk
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
